# A Temporal Graphlet Kernel For Classifying Dissemination in Temporal Graphs ###
Source code for our paper "A Temporal Graphlet Kernel For Classifying Dissemination in Temporal Graphs".

The source code is based on the temporal motif counter from the 
[Stanford Network Analysis Project](https://snap.stanford.edu/index.html).

## Compile
For compilation you need the Eigen3 library. 
Set the environment variable EIGEN3_INCLUDE_DIR to the Eigen3 folder.
For example:

    export EIGEN3_INCLUDE_DIR=/home/user/Downloads/eigen-3.3.7

Next create a folder "release" in "tgkernel". Change into this folder and run

    cmake -DCMAKE_BUILD_TYPE=cmake-build-release ..

Finally, run "make" to compile the code and get the executable file `tgraphlet`.

## Usage

    ./tgraphlet <path to dataset> <mode> [sample size s]
    modes:
    0        TGK-wedge
    1        TGK-star
    2        TGK-all
    3        Approx-s


For example, `./tgraphlet datasetname 2` computes TGK-all for the data set with name `datasetname`.

## Data Sets
See [Benchmark Data Sets for Graph Kernels](https://graphlearning.io/) for data sets.

